Feature: Search for the product
### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario: Search products from schema
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he does not see the results


  @negative
  Scenario: Search test with unavailable product Coffee
    When I call the get search test product endpoint Coffee
    Then verify not found error should be displayed in search results

  @negative
  Scenario: Search test without product
    When I call the get search test endpoint
    Then verify unauthorized error should be displayed in search result