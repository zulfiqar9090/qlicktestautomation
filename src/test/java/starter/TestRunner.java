package starter;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = {"src/test/java/starter/stepdefinitions"},
        features = "src/test/resources/features/products.feature"
)
public class TestRunner {
}
