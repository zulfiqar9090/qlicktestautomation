package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import Common.CommonActions;
import pageObjects.SearchProductActions;
import pageObjects.SearchProductPage;

import java.util.logging.Logger;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;

public class SearchStepDefinitions {


    @Steps
    public CommonActions commonActions;

    @Steps
    SearchProductActions searchProductActions;
    @Steps
    SearchProductPage searchProductPage;

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        searchProductPage.setSearchProduct(arg0);
        SerenityRest.given().get(arg0);
        Logger.getLogger("Calls endpoint with the value "+arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        Logger.getLogger("User sees Result for the Apple product ");
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        Logger.getLogger("User sees Result for the mango product ");
        restAssuredThat(response -> response.body("title", contains("mango")));
    }

    @Then("he does not see the results")
    public void he_Doesn_Not_See_The_Results() {
        Logger.getLogger("User did not sees Result for the product car ");
        restAssuredThat(response -> response.body("error", contains("True")));
    }


    @When("^I call the get search test product endpoint (.*)$")
    public void iCallTheGetSearchProductEndpoint(String product) {

        searchProductActions.searchTestProducts(product);
    }

    @When("I call the get search test endpoint")
    public void iCallTheGetSearchTestEndpoint() {

        searchProductActions.searchTest();
    }

    @Then("verify the search results of product should be displayed")
    public void theSearchResultsOfProductShouldBeDisplayed(){

        commonActions.responseCodeIs(200);
    }

    @Then("verify the product list should not be empty in Search results")
    public void theProductShouldBeDisplayedInSearchResults() {

        commonActions.responseShouldNotBeEmptyList();
    }

    @Then("verify not found error should be displayed in search results")
    public void notFoundErrorShouldBeDisplayedInSearchResult() {
        then().statusCode(404).body("detail.error", is(true));
    }

    @Then("verify unauthorized error should be displayed in search result")
    public void unauthorizedErrorShouldBeDisplayedInSearchResult() {
        then().statusCode(401).body("detail", is("Not authenticated"));
        Logger.getLogger("Un-authorized user step definition ");
    }

}
