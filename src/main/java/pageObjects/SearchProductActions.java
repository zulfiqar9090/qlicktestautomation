package pageObjects;

import Common.CarsAPI;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pageObjects.*;
import net.serenitybdd.rest.SerenityRest;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class SearchProductActions {

    public Response searchTestProducts(String product) {
        Logger.getLogger("Search product "+product);
        return SerenityRest.given().log().uri().spec(CarsAPI.searchReqSpec()).pathParam("product",product ).get("v1/search/test/{product}");
    }

    public Response searchTest() {
        Logger.getLogger("Get Product Response");
        return SerenityRest.given().log().uri().spec(CarsAPI.searchReqSpec()).get("v1/search/test");
    }
}
