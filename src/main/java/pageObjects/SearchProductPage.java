package pageObjects;

public class SearchProductPage {

    String searchProduct;

    public void setSearchProduct(String searchProduct){
        this.searchProduct =searchProduct;
    }

    public String getSearchProduct(){
        return  searchProduct;
    }

}
