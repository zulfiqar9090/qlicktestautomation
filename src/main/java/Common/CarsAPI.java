package Common;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CarsAPI {
public static RequestSpecification searchReqSpec() {
        EnvironmentVariables environmentVariables = Injectors.getInjector()
                .getInstance(EnvironmentVariables.class);

        String baseUrl = (environmentVariables).getProperty("baseurl");

        return new RequestSpecBuilder()
                     .setBaseUri(baseUrl)
                     .setBasePath("api")
                     .setContentType("application/json")
                     .build();
    }

}
